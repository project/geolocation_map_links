# Geolocation Map Links

## Description
The Geolocation Map Links provides a formatter for Geolocation Fields. This formatter allows the display of links to maps from different online map providers.

## Features
- Provides a formatter for Geolocation Fields to link to online maps.
- Supports the following map providers:
- - Google Maps
- - OpenStreetMap
- - Bing Maps
- - HERE WeGo
- - Waze

## Configuration
After installation, you can configure the display of entities with Geolocation Fields to use the Geolocation Map Link formatter.

## Additional Requirements
This module requires the Geolocation Field module to be installed and enabled.

## Recommended modules
The Token module is recommended for use with this module. It allows you to see the available tokens that are available to use on the link title.

## Support
If you encounter any issues or require further assistance, please raise an issue on the module's issue tracker.


