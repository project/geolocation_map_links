<?php

namespace Drupal\geolocation_map_links\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Geolocation Map Link' formatter.
 *
 * @FieldFormatter(
 *   id = "ggeolocation_map_links_formatter",
 *   label = @Translation("Geolocation Map Link"),
 *   field_types = {"geolocation"},
 * )
 */
final class GeolocationMapLinkFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Token Service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs a new GeolocationMapLinkFormatter object.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ModuleHandlerInterface $moduleHandler, Token $token) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->moduleHandler = $moduleHandler;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id, $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = ['zoom' => 10];
    $setting['type'] = 'google';
    $setting['link_title'] = 'View on Map';
    $setting['target'] = '';
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {

    $elements['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Map Type'),
      '#options' => [
        'google' => $this->t('Google Maps'),
        'here' => $this->t('HERE Maps'),
        'bing' => $this->t('Bing Maps'),
        'osm' => $this->t('OpenStreetMap'),
        'waze' => $this->t('Waze'),
      ],
      '#default_value' => $this->getSetting('type'),
    ];

    $elements['zoom'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom Level'),
      '#default_value' => $this->getSetting('zoom'),
    ];

    $elements['link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map Link'),
      '#default_value' => $this->getSetting('link_title'),
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $entity_type = $this->fieldDefinition->getTargetEntityTypeId();

      if ($entity_type === 'taxonomy_term') {
        $entity_type = 'term';
      }

      // Add the token tree UI.
      $elements['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$entity_type, 'geolocation'],
      ];
    }

    $elements['target'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new window'),
      '#return_value' => '_blank',
      '#default_value' => $this->getSetting('target'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Map Type: @type', ['@type' => $this->getSetting('type')]),
      $this->t('Zoom Level: @zoom', ['@zoom' => $this->getSetting('zoom')]),
      $this->t('Link Title: @link_title', ['@link_title' => $this->getSetting('link_title')]),
      $this->t('Open link in new window: @target', ['@target' => $this->getSetting('target') ? $this->t('Yes') : $this->t('No')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    $entity = $items->getEntity();

    $link_title = $this->getSetting('link_title') ?? $this->t('View on Map');

    foreach ($items as $delta => $item) {

      $title = $this->token->replace($link_title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);

      $element[$delta] = [
        '#type' => 'link',
        '#title' => $title,
        '#url' => $this->generateUrl($item->lat, $item->lng, $title),
      ];
    }
    return $element;
  }

  /**
   * Generate the URL for the map link.
   *
   * @param string $lat
   *   The latitude of the location.
   * @param string $lng
   *   The longitude of the location.
   * @param string $title
   *   The title of the link.
   *
   * @return static
   *   A new Url object.
   */
  private function generateUrl($lat, $lng, $title): Url {
    $settings = [
      'type' => $this->getSetting('type') ?? 'google',
      'zoom' => $this->getSetting('zoom'),
      'link_title' => $this->getSetting('link_title') ?? $this->t('View on Map'),
      'target' => $this->getSetting('target'),
    ];

    switch ($settings['type']) {
      case 'google':
        $url = 'https://www.google.com/maps/search/?api=1&query=' . $lat . ',' . $lng;
        break;

      case 'here':
        $url = 'https://share.here.com/l/' . $lat . ',' . $lng . '?p=yes&z=' . $settings['zoom'];
        break;

      case 'osm':
        $url = 'https://www.openstreetmap.org/?mlat=' . $lat . '&mlon=' . $lng . '#map=' . $settings['zoom'] . '/' . $lat . '/' . $lng;
        break;

      case 'waze':
        $url = 'https://waze.com/ul?ll=' . $lat . ',' . $lng . '&z=' . $settings['zoom'];
        break;

      case 'bing':
        $url = 'https://www.bing.com/maps?cp=' . $lat . '~' . $lng . '&sp=point.' . $lat . '_' . $lng . '&lvl=' . $settings['zoom'];
        break;
    }

    $attributes = [];
    if ($settings['target']) {
      $attributes['target'] = $settings['target'];
    }

    return Url::fromUri($url, ['attributes' => $attributes]);

  }

}
